#include "Admin_UI.h"

Admin_UI::Admin_UI() : User_UI() {}

bool Admin_UI::addEvent(string name, string date, string venue, int numPlaces) {
	Event* e = getEventHandler()->createEvent(name, date, venue, numPlaces);
	if (!getEventHandler()->findEvent(e)) {
		getEventHandler()->addEvent(e);
		return true;
	}
	else {
		return false;
	}
}

bool Admin_UI::deleteEvent(int eventID) {
	int i = -1;
	i = getEventHandler()->getEventIndex(eventID);
	if (i >= 0) {
		getBookingHandler()->deleteBookings(eventID);
		getEventHandler()->deleteEvent(i);
		return true;
	}
	else {
		return false;
	}
}

void Admin_UI::displayAllEventsAdmin() {
	this->displayAllEvents();
}

void Admin_UI::displayEventDetailsAdmin(int eventID) {
	this->displayEventDetails(eventID);
}