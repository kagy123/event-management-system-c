#include "User_UI.h"

class Admin_UI : public User_UI {

public:
	Admin_UI();
	bool addEvent(string name, string date, string venue, int numPlaces);
	bool deleteEvent(int eventID);
	void displayAllEventsAdmin();
	void displayEventDetailsAdmin(int eventID);
};