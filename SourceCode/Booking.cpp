#include "Booking.h"

int Booking::nextBookingID = 1;

Booking::Booking(int eventID) {
	this->bookingID = nextBookingID++;
	this->eventID = eventID;
}

int Booking::getEventID() {
	return eventID;
}


int Booking::getBookingID() {
	return bookingID;
}