using namespace std;

class Booking {

private:
	int bookingID;
	static int nextBookingID;
	int eventID;

public:
	Booking(int eventID);
	~Booking();
	int getEventID();
	int getBookingID();
};
