#include "BookingHandler.h"

BookingHandler* BookingHandler::bookingHandler;

BookingHandler::BookingHandler() {
	this->bookings = new vector<Booking*>();
}

BookingHandler* BookingHandler::getBookingHandler() {
	if (!bookingHandler) {
		bookingHandler = new BookingHandler();
		return bookingHandler;
	}
	else {
		return bookingHandler;
	}
}

vector<Booking*>* BookingHandler::getBookings() {
	return bookings;
}

Booking* BookingHandler::createBooking(int eventID) {
	return new Booking(eventID);
}

void BookingHandler::addBooking(Booking* booking) {
	bookings->push_back(booking);
}

void BookingHandler::deleteBooking(int index) {
	bookings->erase(bookings->begin() + index);
}

void BookingHandler::deleteBookings(int eventID) {
	int i = 0;
	for (Booking* b : *bookings) {
		if (b->getEventID() == eventID) {
			bookings->erase(bookings->begin() + i);
			i++;
		}
	}
}

int BookingHandler::findBooking(int eventID) {
	int i = 0;
	for (Booking* b : *bookings) {
		if (b->getEventID() == eventID) {
			return i;
		}
		i++;
	}
	return -1;
}