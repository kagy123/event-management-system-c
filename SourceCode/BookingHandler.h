#include "Booking.h"
#include <vector>

class BookingHandler {

private:
	vector<Booking*>* bookings;
	static BookingHandler* bookingHandler;
	BookingHandler();

public:
	static BookingHandler* getBookingHandler();
	vector<Booking*>* getBookings();
	Booking* createBooking(int eventID);
	void addBooking(Booking* booking);
	void deleteBooking(int index);
	void deleteBookings(int eventID);
	int findBooking(int eventID);
};