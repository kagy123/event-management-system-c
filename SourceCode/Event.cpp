#include "Event.h"
#include "sstream"

int Event::nextEventID = 1;

Event::Event(string name, string date, string venue, int numPlaces){
	this->name = name;
	this->date = date;
	this->venue = venue;
	this->numPlaces = numPlaces;
	this->numBookedPlaces = 0;
	this->eventID = nextEventID++;
}

Event::~Event() {
}

string Event::getName() {
	return name;
}

string Event::getDate() {
	return date;
}

string Event::getVenue() {
	return venue;
}

int Event::getEventID() {
	return eventID;
}

int Event::getNumPlaces() {
	return numPlaces;
}

int Event::getNumBookedPlaces() {
	return numBookedPlaces;
}

int Event::getNumAvailablePlaces() {
	return numPlaces - numBookedPlaces;
}

string Event::placesAvailable() {
	if (numPlaces == numBookedPlaces) {
		return "Sold out";
	}
	else {
		return "Places available";
	}
}

string Event::getDetails() {
	stringstream ss;
	ss << getEventID() << " | " << getName() << " | " << getDate() << " | " << placesAvailable();
	return ss.str();
}

string Event::getAllDetails() {
	stringstream ss;
	ss << "Name: " << getName() << "\nDate: " << getDate() << "\nVenue: " << getVenue() << "\nTotal: " << getNumPlaces()
		<< "\nBooked: " << getNumBookedPlaces() << "\nAvailable: " << getNumAvailablePlaces();
	return ss.str();
}

string Event::getDetailsForBooking() {
	stringstream ss;
	ss << getEventID() << " | " << getName() << " | " << getDate() << " | " << getVenue();
	return ss.str();
}

bool Event::placesAvailable(int num) {
	if (num <= getNumAvailablePlaces()) {
		return true;
	}
	else {
		return false;
	}
}

void Event::addBookedPlaces(int num) {
	numBookedPlaces += num;
}

void Event::deleteBookedPlace() {
	numBookedPlaces--;
}