#include <string>
using namespace std;

class Event {

private:
	string name;
	string date;
	string venue;
	int eventID;
	static int nextEventID;
	int numPlaces;
	int numBookedPlaces;
	string placesAvailable();

public:
	Event(string name, string date, string venue, int numPlaces);
	~Event();
	string getName();
	string getDate();
	string getVenue();
	int getEventID();
	int getNumPlaces();
	int getNumBookedPlaces();
	int getNumAvailablePlaces();
	string getDetails();
	string getAllDetails();
	string getDetailsForBooking();
	bool placesAvailable(int num);
	void addBookedPlaces(int num);
	void deleteBookedPlace();
};