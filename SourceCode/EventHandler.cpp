#include "EventHandler.h"

EventHandler* EventHandler::eventHandler;

EventHandler::EventHandler() {
	this->events = new vector < Event* >();
}

EventHandler* EventHandler::getEventHandler() {
	if (!eventHandler) {
		eventHandler = new EventHandler();
		return eventHandler;
	}
	else {
		return eventHandler;
	}
}

Event* EventHandler::createEvent(string name, string date, string venue, int numPlaces) {
	return new Event(name, date, venue, numPlaces);
}

vector<Event*>* EventHandler::getEvents() {
	return events;
}

Event* EventHandler::findEvent(int eventID) {
	for (Event* e : *events) {
		if (e->getEventID() == eventID) {
			return e;
		}
	}
	return nullptr;
}

bool EventHandler::findEvent(Event* event) {
	for (Event* e : *events) {
		if ((e->getName() == event->getName() && e->getVenue() == event->getVenue() 
			&& e->getDate() == event->getDate() || e == event)) {
			return true;
		}
	}
	return false;
}

int EventHandler::getEventIndex(int eventID) {
	int i = 0;
	for (Event* e : *events) {
		if (e->getEventID() == eventID) {
			return i;
		}
		i++;
	}
	return -1;
}

void EventHandler::addEvent(Event* event) {
	events->push_back(event);
}

void EventHandler::deleteEvent(int index) {
	events->erase(events->begin() + index);
}