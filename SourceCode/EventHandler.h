#include "Event.h"
#include <vector>

class EventHandler {

private:
	vector<Event*>* events;
	static EventHandler* eventHandler;
	EventHandler();

public:
	static EventHandler* getEventHandler();
	Event* createEvent(string name, string date, string venue, int numPlaces);
	vector<Event*>* getEvents();
	Event* findEvent(int eventID);
	int getEventIndex(int eventID);
	bool findEvent(Event* event);
	void addEvent(Event* event);
	void deleteEvent(int index);
};