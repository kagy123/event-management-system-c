#include "EventManagerApp.h"
#include <sstream>

Admin_UI* admin;
GeneralUser_UI* user;

void main() {
	admin = new Admin_UI();
	user = new GeneralUser_UI();
	int choice = -2;
	int num;
	int num2;
	string name, date, venue;

	//hardcoded events
	admin->addEvent("Movies", "30/05/2017", "Stoke", 10);
	admin->addEvent("Pool Tourney", "10/01/2015", "Staffordshire", 5);
	user->bookPlace(2, 2);

	EventManagerApp::displayHeading();
	while (choice != -1) {
		EventManagerApp::displayMenu();
		cout << endl;
		choice = EventManagerApp::getValidChoice();
		cout << endl;

		switch (choice) {
		case 1:
			cout << "Enter: " << endl;
			cout << "     Event name: ";
			getline(cin, name);
			cout << "     Date: ";
			getline(cin, date);
			cout << "     Venue: ";
			getline(cin, venue);
			cout << "     Number of places: ";
			num = EventManagerApp::getValidInteger();
			while (num <= 0) {
				cout << "     The number must be possitive." << endl;
				cout << "     Number of places: ";
				num = EventManagerApp::getValidInteger();
			}
			if (admin->addEvent(name, date, venue, num)) {
				cout << "Event has been added." << endl << endl;
			}
			else {
				cout << "Event has not been added./The same event probably exists." << endl << endl;
			}
			break;
		case 2:
			if (!EventHandler::getEventHandler()->getEvents()->empty()) {
				EventManagerApp::displayEvents();
				cout << "Enter event ID: ";
				num = EventManagerApp::getValidInteger();
				cout << endl;
				if (admin->deleteEvent(num)) {
					cout << "Event has been deleted." << endl << endl;
				}
				else {
					cout << "Event has not been deleted./Event not found." << endl << endl;
				}
			}
			else {
				cout << "There are no events." << endl << endl;
			}
			break;
		case 3:
			if (!EventHandler::getEventHandler()->getEvents()->empty()) {
				EventManagerApp::displayEvents();
				cout << "Enter event ID: ";
				num = EventManagerApp::getValidInteger();
				cout << "Number of places to book: ";
				num2 = EventManagerApp::getValidInteger();
				while (num <= 0) {
					cout << "     The number must be possitive." << endl;
					cout << "     Number of places: ";
					num = EventManagerApp::getValidInteger();
				}
				if (user->bookPlace(num, num2)) {
					cout << endl << "/nPlace/s has/have been booked." << endl << endl;
				}
				else {
					cout << endl << "Place/s has/have not been booked." << endl << endl;
				}
			}
			else {
				cout << "There are no events available." << endl << endl;
			}
			break;
		case 4:
			if (!BookingHandler::getBookingHandler()->getBookings()->empty()) {
				EventManagerApp::viewBookings();
				cout << "Enter event ID: ";
				num = EventManagerApp::getValidInteger();
				cout << endl;
				if (user->deletePlace(num)) {
					cout << "Booking has been cancelled." << endl << endl;
				}
				else {
					cout << "Booking has not been cancelled./Booking not found." << endl << endl;
				}
			}
			else {
				cout << "You do not have any bookings." << endl << endl;
			}
			break;
		case 5:
			if (!BookingHandler::getBookingHandler()->getBookings()->empty()) {
				EventManagerApp::viewBookings();
			}
			else {
				cout << "You do not have any bookings." << endl << endl;
			}
			break;
		case 6:
			if (!EventHandler::getEventHandler()->getEvents()->empty()) {
				EventManagerApp::displayEvents();
			}
			else {
				cout << "There are no events." << endl << endl;
			}
			break;
		case 7:
			if (!EventHandler::getEventHandler()->getEvents()->empty()) {
				EventManagerApp::displayEvents();
				cout << "Enter event ID: ";
				num = EventManagerApp::getValidInteger();
				cout << endl;
				admin->displayEventDetailsAdmin(num);
				cout << endl;
			}
			else {
				cout << "There are no events." << endl << endl;
			}			
			break;
		case 0:
			cout << endl << "Goodbye and have a nice day." << endl << endl;
			choice = -1;
			break;
		default:
			cout << "Wrong number. Please try again." << endl << endl;
			break;
		}
	}
}

void EventManagerApp::displayHeading() {
	cout << "***********************************************" << endl;
	cout << "*           Event Management System           *" << endl;
	cout << "***********************************************" << endl;
}

void EventManagerApp::displayMenu() {
	cout << "Admin:" << endl;
	cout << "     1.Add event." << endl;
	cout << "     2.Delete event." << endl;
	cout << "User:" << endl;
	cout << "     3.Book a place." << endl;
	cout << "     4.Cancel booking." << endl;
	cout << "     5.View bookings" << endl;
	cout << "Both:" << endl;
	cout << "     6.Display all events." << endl;
	cout << "     7.Display event details." << endl;
	cout << "     0.Exit" << endl;
}

int EventManagerApp::getValidChoice() {
	int num = 0;
	while (true) {
		string input = "";
		cout << "Choice: ";
		getline(cin, input);
		stringstream ss(input);
		if (ss >> num)
			break;
		cout << "Invalid number. Please try again." << endl;
	}
	return num;
}

int EventManagerApp::getValidInteger() {
	int num = 0;
	while (true) {
		string input = "";
		getline(cin, input);
		stringstream ss(input);
		if (ss >> num)
			break;
		cout << "Invalid number. Please try again." << endl;
	}
	return num;
}

void EventManagerApp::displayEvents() {
	cout << "ID  |  Event name  |  Date  |  Available/Sold Out" << endl << endl;
	admin->displayAllEventsAdmin();
	cout << endl;
}

void EventManagerApp::viewBookings() {
	cout << "Event ID  |  Event name  |  Date  |  Event venue" << endl << endl;
	user->viewBookings();
	cout << endl;
}