#include "Admin_UI.h"
#include "GeneralUser_UI.h"

class EventManagerApp{

public:
	EventManagerApp();
	void main();
	static void displayHeading();
	static void displayMenu();
	static int getValidChoice();
	static int getValidInteger();
	static void displayEvents();
	static void viewBookings();
};