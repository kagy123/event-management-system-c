#include "GeneralUser_UI.h"

GeneralUser_UI::GeneralUser_UI() : User_UI(){}

bool GeneralUser_UI::bookPlace(int eventID, int numPlaces) {
	Event* e = EventHandler::getEventHandler()->findEvent(eventID);
	if (e && !(numPlaces <= 0)) {
		if (e->placesAvailable(numPlaces)) {
			for (int i = 0; i < numPlaces; i++) {
				Booking* b = BookingHandler::getBookingHandler()->createBooking(eventID);
				BookingHandler::getBookingHandler()->addBooking(b);
			}
			e->addBookedPlaces(numPlaces);
			return true;
		}
		else {
			return false;
		}
	}
	else {
		return false;
	}
}

bool GeneralUser_UI::deletePlace(int eventID) {
	Event* e = EventHandler::getEventHandler()->findEvent(eventID);
	if (e) {
		int index = BookingHandler::getBookingHandler()->findBooking(eventID);
		if (index != -1) {
			BookingHandler::getBookingHandler()->deleteBooking(index);
			e->deleteBookedPlace();
			return true;
		}
		else {
			return false;
		}
	}
	else {
		return false;
	}
}

void GeneralUser_UI::displayAllEventsGeneral() {
	this->displayAllEvents();
}

void GeneralUser_UI::displayEventDetailsGeneral(int eventID) {
	this->displayEventDetails(eventID);
}

void GeneralUser_UI::viewBookings() {
	for (Booking* b : *BookingHandler::getBookingHandler()->getBookings()) {
		cout << EventHandler::getEventHandler()->findEvent(b->getEventID())->getDetailsForBooking() << endl;
	}
}