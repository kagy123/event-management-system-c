#include "User_UI.h"

class GeneralUser_UI : public User_UI {

public:
	GeneralUser_UI();
	bool bookPlace(int eventID, int numPlaces);
	bool deletePlace(int eventID);
	void viewBookings();
	void displayAllEventsGeneral();
	void displayEventDetailsGeneral(int eventID);
};