#include "User_UI.h"

User_UI::User_UI() {}

EventHandler* User_UI::getEventHandler() {
	return EventHandler::getEventHandler();
}

BookingHandler* User_UI::getBookingHandler() {
	return BookingHandler::getBookingHandler();
}

void User_UI::displayAllEvents() {
	for (Event* e : *getEventHandler()->getEvents()) {
		cout << e->getDetails() << endl;
	}
}

void User_UI::displayEventDetails(int eventID) {
	Event* e = getEventHandler()->findEvent(eventID);
	if (e) {
		cout << e->getAllDetails() << endl;
	}
	else {
		cout << "Event has not been found." << endl;
	}
}