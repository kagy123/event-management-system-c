#ifndef USER_UI_H
#define USER_UI_H
#include "BookingHandler.h"
#include "EventHandler.h"
#include <iostream>
#include <string>
using namespace std;

class User_UI {

protected:
	User_UI();
	EventHandler* getEventHandler();
	BookingHandler* getBookingHandler();
	void displayAllEvents();
	void displayEventDetails(int eventID);
};

#endif